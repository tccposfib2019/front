import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('formacao', [
        'formacoesCombo',
    ]),

    methods: {
        ...mapActions('formacao', [
            'getAllFormacoes',
        ]),
    },
};