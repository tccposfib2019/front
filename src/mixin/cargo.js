import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('cargo', [
        'cargosCombo',
    ]),

    methods: {
        ...mapActions('cargo', [
            'getAllCargos',
        ]),
    },
};