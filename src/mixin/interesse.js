import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('interesse', [
        'interessesCombo',
    ]),

    methods: {
        ...mapActions('interesse', [
            'getAllInteresses',
        ]),
    },
};