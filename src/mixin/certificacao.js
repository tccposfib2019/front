import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('certificacao', [
        'certificacoesCombo',
    ]),

    methods: {
        ...mapActions('certificacao', [
            'getAllCertificacoes',
        ]),
    },
};