import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('common', [
        'messages',
        'fetching',
    ]),

    methods: {
        ...mapActions('common', [
            'setFetching',
            'setMessage',
            'resetMessages',
        ]),
    },
};