import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('projeto', [
        'projetosCombo',
    ]),

    methods: {
        ...mapActions('projeto', [
            'getAllProjetos',
        ]),
    },
};