import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('habilidade', [
        'habilidadesCombo',
    ]),

    methods: {
        ...mapActions('habilidade', [
            'getAllHabilidades',
        ]),
    },
};