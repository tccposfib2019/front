import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('empresa', [
        'empresasCombo',
    ]),

    methods: {
        ...mapActions('empresa', [
            'getAllEmpresas',
        ]),
    },
};