import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('filter', [
        'filterData',
        'filteredPeople',
    ]),

    methods: {
        ...mapActions('filter', [
            'getAll',
        ]),
    },
};