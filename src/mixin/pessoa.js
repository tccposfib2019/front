import { mapActions, mapGetters } from 'vuex';

export default {
    computed: mapGetters('pessoa', [
        'pessoaData',
    ]),

    methods: {
        ...mapActions('pessoa', [
            'getById',
        ]),
    },
};