import Vue from "vue";
import ElementUI from 'element-ui';
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import store from './store/index';

Vue.config.productionTip = false;

Vue.use(ElementUI);
Vue.use(Argon);

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount("#app");