import Vue from 'vue';
import Vuex from 'vuex';
import common from './modules/common';
import habilidade from './modules/habilidade';
import filter from './modules/filter';
import certificacao from './modules/certificacao';
import empresa from './modules/empresa';
import cargo from './modules/cargo';
import projeto from './modules/projeto';
import interesse from './modules/interesse';
import formacao from './modules/formacao';
import pessoa from './modules/pessoa';

Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        common,
        habilidade,
        filter,
        certificacao,
        empresa,
        cargo,
        projeto,
        interesse,
        formacao,
        pessoa,
    },
});