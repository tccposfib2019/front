import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        filterData: {
            Nome: '',
            habilidades: [],
            certificaoes: [],
            empresas: [],
            cargos: [],
            projetos: [],
            interesses: [],
            formacoes: [],
        },


        filteredPeople: [
        ],
    },

    getters: {
        filterData: state => state.filterData,
        filteredPeople: state => state.filteredPeople,
    },

    actions: {
        async getAll({ commit, dispatch, state }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/ListarPessoasFiltro?ids=${state.filterData.habilidades[0]}`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_FILTERED_PEOPLE', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar habilidade.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'RESET_FILTER_DATA' (state) {
            state.filterData = {
                habilidades: [],
                certificaoes: [],
                empresas: [],
                cargos: [],
                projetos: [],
                interesses: [],
                formacoes: [],
            };
        },

        'SET_FILTERED_PEOPLE' (state, payload) {
            state.filteredPeople = payload;
        },
    },
};