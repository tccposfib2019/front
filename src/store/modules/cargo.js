import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        cargosCombo: [],
    },

    getters: {
        cargosCombo: state => state.cargosCombo,
    },

    actions: {
        async getAllCargos({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/Cargo`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_CARGO_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar cargo.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_CARGO_COMBO' (state, payload) {
            state.cargosCombo = payload;
        },

        'RESET_CARGO_COMBO' (state, payload) {
            state.cargosCombo = [];
        },
    },
};