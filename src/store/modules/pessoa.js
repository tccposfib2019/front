import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        pessoaData: [],
    },

    getters: {
        pessoaData: state => state.pessoaData,
    },

    actions: {
        async getById({ commit, dispatch }, payload) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/PessoaDetalhe?id=${payload}`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_PESSOA', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar cargo.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_PESSOA' (state, payload) {
            state.pessoaData = payload;
        },

        'RESET_PESSOA' (state) {
            state.pessoaData = [];
        },
    },
};