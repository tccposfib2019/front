export default {
    namespaced: true,

    state: {
        messages: {
            success: '',
            error: [],
            warning: '',
            validation: [],
        },
        fetching: false,
    },

    getters: {
        messages: state => state.messages,
        fetching: state => state.fetching,
    },

    actions: {
        setFetching({ commit }, payload) {
            commit('MAIN_SET_FETCHING', payload);
        },

        setMessage({ commit }, payload) {
            commit('MAIN_SET_MESSAGE', payload);
        },

        resetMessages({ commit }) {
            commit('MAIN_SET_MESSAGE', { type: 'success', message: '' });
            commit('MAIN_SET_MESSAGE', { type: 'error', message: [] });
            commit('MAIN_SET_MESSAGE', { type: 'warning', message: '' });
            commit('MAIN_SET_MESSAGE', { type: 'validation', message: [] });
        },
    },

    mutations: {
        'MAIN_SET_FETCHING' (state, payload) {
            state.fetching = payload;
        },

        'MAIN_SET_MESSAGE' (state, payload) {
            state.messages[payload.type] = payload.message;
        },
    },
};