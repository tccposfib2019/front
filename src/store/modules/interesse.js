import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        interessesCombo: [],
    },

    getters: {
        interessesCombo: state => state.interessesCombo,
    },

    actions: {
        async getAllInteresses({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/Interesses`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_INTERESSE_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar empresa.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_INTERESSE_COMBO' (state, payload) {
            state.interessesCombo = payload;
        },

        'RESET_INTERESSE_COMBO' (state, payload) {
            state.interessesCombo = [];
        },
    },
};