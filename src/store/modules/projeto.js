import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        projetosCombo: [],
    },

    getters: {
        projetosCombo: state => state.projetosCombo,
    },

    actions: {
        async getAllProjetos({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/Projeto`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_PROJETO_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar projeto.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_PROJETO_COMBO' (state, payload) {
            state.projetosCombo = payload;
        },

        'RESET_PROJETO_COMBO' (state, payload) {
            state.projetosCombo = [];
        },
    },
};