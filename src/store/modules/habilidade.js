import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        habilidadesCombo: [],
    },

    getters: {
        habilidadesCombo: state => state.habilidadesCombo,
    },

    actions: {
        async getAllHabilidades({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/Habilidades`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_HABILIDADE_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar habilidade.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_HABILIDADE_COMBO' (state, payload) {
            state.habilidadesCombo = payload;
        },

        'RESET_HABILIDADE_COMBO' (state) {
            state.habilidadesCombo = [];
        },
    },
};