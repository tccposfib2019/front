import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        certificacoesCombo: [],
    },

    getters: {
        certificacoesCombo: state => state.certificacoesCombo,
    },

    actions: {
        async getAllCertificacoes({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/Certificao`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_CERTIFICAO_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar CERTIFICAO.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_CERTIFICAO_COMBO' (state, payload) {
            state.certificacoesCombo = payload;
        },

        'RESET_CERTIFICAO_COMBO' (state) {
            state.certificacoesCombo = [];
        },
    },
};