import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        empresasCombo: [],
    },

    getters: {
        empresasCombo: state => state.empresasCombo,
    },

    actions: {
        async getAllEmpresas({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/Empresa`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_EMPRESA_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar empresa.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_EMPRESA_COMBO' (state, payload) {
            state.empresasCombo = payload;
        },

        'RESET_EMPRESA_COMBO' (state, payload) {
            state.empresasCombo = [];
        },
    },
};