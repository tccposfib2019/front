import axios from 'axios';
import { apiUrl } from '../../config/config';

export default {
    namespaced: true,

    state: {
        formacoesCombo: [],
    },

    getters: {
        formacoesCombo: state => state.formacoesCombo,
    },

    actions: {
        async getAllFormacoes({ commit, dispatch }) {
            dispatch('common/setFetching', true, { root: true });
            await axios.get(`${apiUrl}/FormacaoAcademica`).then((response) => {
                dispatch('common/setFetching', false, { root: true });
                commit('SET_FORMACAO_COMBO', response.data);
            }).catch((error) => {
                dispatch('common/setFetching', false, { root: true });
                if (error.response.data.stackTrace) {
                    dispatch('common/setMessage', {
                        type: 'error',
                        message: 'Erro ao carregar empresa.',
                    }, { root: true });
                }
            });
        },

    },

    mutations: {
        'SET_FORMACAO_COMBO' (state, payload) {
            state.formacoesCombo = payload;
        },

        'RESET_FORMACAO_COMBO' (state) {
            state.formacoesCombo = [];
        },
    },
};